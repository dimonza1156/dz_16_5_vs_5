#include <iostream>
#include <windows.h>

int main()
{
    SYSTEMTIME a;
    GetLocalTime(&a);
    std::cout << "Date: " << a.wDay << ".";
    std::cout << a.wMonth << ".";
    std::cout << a.wYear << "\n \n";
    int day = a.wDay;

    const int N = 10;
    int array[N][N] = {};

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            array[i][j] = i + j;
            std::cout << array[i][j];

        }
        std::cout << "\n";
    }

    int index = 0;
    index = day % N;
    int sum = 0;
    std::cout << "\nSum: ";

    for (int j = 0; j < N; j++)
    {
        sum += array[index][j];
    }
    std::cout << sum << "\n";
    return 0;
}
